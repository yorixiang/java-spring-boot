package com.demo.bean;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Range;

public class Staff {
	
	private String staffID;
	
	@NotEmpty
	@Range(max = 250, min = 100, message = "have to between 250 to 100")
	private String staffHeight;
	
	@NotEmpty
	@Range(max = 150, min = 30, message = "have to between 150 to 30")
	private String staffWeight;
	
	@NotEmpty
	@Pattern(regexp = "[a-zA-Z]{1,20}-[a-zA-Z]{1,20}")
	private String staffEnglishName;
	
	@NotEmpty
	@Pattern(regexp = "[\\u4E00-\\u9FA5]+")
	private String staffChineseName;
	
	@NotEmpty
	@Digits(integer = 4, fraction = 0)
	private String staffExt;
	
	@NotEmpty
	@Email
	private String staffEmail;
	
	@NotEmpty
	private String staffBMI;
	
	private Record record;
	
	public Staff() {
		
	}
	
	public Staff (String staffID, String staffHeight, String staffWeight, String staffEnglishName, String staffChineseName, String staffExt, String staffEmail, String staffBMI) {
		this.staffID = staffID;
		this.staffHeight = staffHeight;
		this.staffWeight = staffWeight;
		this.staffEnglishName = staffEnglishName;
		this.staffChineseName = staffChineseName;
		this.staffExt = staffExt;
		this.staffEmail = staffEmail;
		this.staffBMI = staffBMI;
	}

	public String getStaffID() {
		return staffID;
	}

	public void setStaffID(String staffID) {
		this.staffID = staffID;
	}

	public String getStaffHeight() {
		return staffHeight;
	}

	public void setStaffHeight(String staffHeight) {
		this.staffHeight = staffHeight;
	}

	public String getStaffWeight() {
		return staffWeight;
	}

	public void setStaffWeight(String staffWeight) {
		this.staffWeight = staffWeight;
	}

	public String getStaffEnglishName() {
		return staffEnglishName;
	}

	public void setStaffEnglishName(String staffEnglishName) {
		this.staffEnglishName = staffEnglishName;
	}

	public String getStaffChineseNmae() {
		return staffChineseName;
	}

	public void setStaffChineseNmae(String staffChineseName) {
		this.staffChineseName = staffChineseName;
	}

	public String getStaffExt() {
		return staffExt;
	}

	public void setStaffExt(String staffExt) {
		this.staffExt = staffExt;
	}

	public String getStaffEmail() {
		return staffEmail;
	}

	public void setStaffEmail(String email) {
		this.staffEmail = email;
	}

	public String getStaffBMI() {
		return staffBMI;
	}

	public void setStaffBMI(String staffBMI) {
		this.staffBMI = staffBMI;
	}
	
	public Record getRecord() {
		return record;
	}

	public void setRecord(Record record) {
		this.record = record;
	}

}
