package com.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.demo.bean.Record;
import com.demo.bean.Staff;

public class StaffRowapper implements RowMapper<Staff> {

	public Staff mapRow(ResultSet rs, int rowNum) throws SQLException {

		Staff staff = new Staff(
				rs.getString("staffID"),
				rs.getString("staffHeight"),
				rs.getString("staffWeight"),
				rs.getString("staffEnglishName"),
				rs.getString("staffChineseName"),
				rs.getString("staffExt"),
				rs.getString("staffEmail"),
				rs.getString("staffBMI"));
		
		Record record = new Record();
		record.setCreateTime(rs.getString("createDateTime"));
		record.setUpdateTime(rs.getString("updateDateTime"));
		
		staff.setRecord(record);
		
		return staff;
	}

}
