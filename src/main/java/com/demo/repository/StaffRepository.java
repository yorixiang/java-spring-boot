package com.demo.repository;

import java.util.ArrayList;
import java.util.List;

//import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.demo.bean.Staff;

@Repository
public class StaffRepository {

	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;
	
	private static final String CREATESQL = 
			"INSERT INTO staffTable (staffHeight, staffWeight, "
			+ "staffEnglishName, staffChineseName, staffExt, staffEmail, staffBMI) "
			+ "values (:staffHeight, :staffWeight, :staffEnglishName, :staffChineseName, "
			+ ":staffExt, :staffEmail, :staffBMI)";
	
	private static final String READSQL = 
			"SELECT * FROM staffTable WHERE staffID = :staffID";
	
	private static final String UPDATESQL = 
			"UPDATE staffTable SET staffHeight = :staffHeight,"
			+ "staffWeight = :staffWeight, staffEnglishName = :staffEnglishName,"
			+ "staffChineseName = :staffChineseName, staffExt = :staffExt,"
			+ "staffEmail = :staffEmail, staffBMI = :staffBMI WHERE staffID = :staffID";
	
	private static final String DELETESQL = 
			"DELETE FROM staffTable WHERE staffID = :staffID";
	
	private static final String QUERYALL = "SELECT * FROM staffTable";
	
	/**
	 * read data
	 * @param staffID
	 * @return
	 */
	public List<Staff> readData(int staffID) {
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource("staffID", staffID);
		return namedJdbcTemplate.query(READSQL, mapSqlParameterSource, new StaffRowapper());
	}
	
	/**
	 * create data
	 * @param staff
	 * @return
	 */
	public boolean createData(Staff staff) {
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("staffHeight", staff.getStaffHeight())
		.addValue("staffWeight", staff.getStaffWeight())
		.addValue("staffEnglishName", staff.getStaffEnglishName())
		.addValue("staffChineseName", staff.getStaffChineseNmae())
		.addValue("staffExt", staff.getStaffExt())
		.addValue("staffEmail", staff.getStaffEmail())
		.addValue("staffBMI", staff.getStaffBMI());
		return namedJdbcTemplate.update(CREATESQL, mapSqlParameterSource)>0;
	}
	
	/**
	 * update data
	 * @param staff
	 * @return
	 */
	public boolean updateData(Staff staff) {
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("staffHeight", staff.getStaffHeight())
		.addValue("staffWeight", staff.getStaffWeight())
		.addValue("staffEnglishName", staff.getStaffEnglishName())
		.addValue("staffChineseName", staff.getStaffChineseNmae())
		.addValue("staffExt", staff.getStaffExt())
		.addValue("staffEmail", staff.getStaffEmail())
		.addValue("staffBMI", staff.getStaffBMI())
		.addValue("staffID", staff.getStaffID());
		return namedJdbcTemplate.update(UPDATESQL, mapSqlParameterSource)>0;
	}
	
	/**
	 * delete data
	 * @param staffID
	 * @return
	 */
	public boolean deleteData(int staffID) {
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource("staffID", staffID);
		return namedJdbcTemplate.update(DELETESQL, mapSqlParameterSource)>0;
		
	}
	
	/**
	 * query all data
	 * @return
	 */
	public List<Staff> queryAll() {
		List<Staff> staffList = new ArrayList<Staff>();
		staffList = namedJdbcTemplate.query(QUERYALL, new StaffRowapper());
		return staffList;
	}

	
}
