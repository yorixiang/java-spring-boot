package com.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.bean.Staff;
import com.demo.service.StaffService;

@RestController
@RequestMapping("/jdbctemplate/staff")
public class StaffController {

	@Autowired
	private StaffService staffService;
	
	private static final Logger logger = LogManager.getLogger(StaffController.class);

	@GetMapping("/")
	public List<Staff> read(@Valid @RequestParam int staffID) {
		return staffService.readData(staffID);
	}

	@PostMapping("/")
	public boolean create(@Valid @RequestBody Staff staff) {
		return staffService.createData(staff);
	}

	@PutMapping("/")
	public boolean update(@Valid @RequestBody Staff staff) {
		return staffService.updateData(staff);
	}

	@DeleteMapping("/")
	public boolean delete(@Valid @RequestParam int staffID) {
		return staffService.deleteData(staffID);
	}

	@GetMapping("/download")
	public ResponseEntity<byte[]> download(HttpServletResponse response) {

		String staffCSV = staffService.downloadToCSV();
		logger.info("get data {}", staffCSV);
		byte[] staffBytes = staffCSV.getBytes();
		String fileName = "employeeJDBC.csv";
		HttpHeaders httpHeader = new HttpHeaders();
		httpHeader.setContentLength(staffBytes.length);
		httpHeader.setContentType(new MediaType("text", "csv"));
		httpHeader.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
		return new ResponseEntity<byte[]>(staffBytes, httpHeader, HttpStatus.OK);
	}

}
