package com.demo.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.demo.bean.Staff;
import com.demo.service.MybatisService;
import com.demo.service.StaffBatchService;

@RestController
@RequestMapping("/mybatis/staff")
public class MybatisController {
	
	private static final Logger logger = LogManager.getLogger(MybatisController.class);

	@Autowired
	private MybatisService mybatisService;

	@Autowired
	private StaffBatchService staffBatchService;

	@GetMapping("/")
	public Staff readData(@Valid @RequestParam int staffID) {
		return mybatisService.readData(staffID);
	}

	@PostMapping("/")
	public boolean create(@Valid @RequestBody Staff staff) {
		return mybatisService.createData(staff);
	}

	@PutMapping("/")
	public boolean update(@Valid @RequestBody Staff staff) {
		return mybatisService.updateData(staff);
	}

	@DeleteMapping("/")
	public boolean delete(@Valid @RequestParam int staffID) {
		return mybatisService.deleteData(staffID);
	}
	
	@PostMapping("/upload")
	public void batchInsert(@RequestParam("file") MultipartFile file) {
		
		List<String> staffArrayList = new ArrayList<>();
		
		if (!file.isEmpty()) {
			logger.info("file is not empty");
			
			try (BufferedReader bufferedReader = 
					new BufferedReader(new InputStreamReader(file.getInputStream(),  StandardCharsets.UTF_8))) {
				
				String line = null;
			    while ((line = bufferedReader.readLine()) != null) {
			    	staffArrayList.add(line);
			    }
			    
			} catch (IOException e) {
				logger.error("file: {}, error: {}", file.getName(), e);
			}
			
			staffBatchService.batchInsert(staffArrayList);
		}
	}

}
