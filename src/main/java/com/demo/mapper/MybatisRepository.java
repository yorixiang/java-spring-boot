package com.demo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.demo.bean.Staff;

@Mapper
public interface MybatisRepository {

	/**
	 * read data by staffID
	 * 
	 * @param staffID
	 * @return
	 */
    Staff readData(int staffID);
    
    /**
     * create new staff
     * 
     * @param staff
     * @return
     */
    boolean createData(Staff staff);
    
    /**
     * update staff data
     * 
     * @param staff
     * @return
     */
    boolean updateData(Staff staff);
    
    /**
     * delete staff data
     * 
     * @param staffID
     * @return
     */
    boolean deleteData(int staffID);
    
    void batchInsert(@Param("batchInsert") List<Staff> staffList);
    
}
