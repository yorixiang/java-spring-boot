package com.demo.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.bean.Staff;
import com.demo.mapper.MybatisRepository;

@Service
public class MybatisService {

	@Autowired
	private MybatisRepository mybatisRepository;
	
	private static final Logger logger = LogManager.getLogger(MybatisService.class);
	
	/**
	 * 
	 * @param staffID
	 * @return
	 */
	public Staff readData(int staffID) {
		return mybatisRepository.readData(staffID);
	}
	
	/**
	 * 
	 * @param staff
	 * @return
	 */
	public boolean createData(Staff staff) {
		
//		calculate BMI
		int height = Integer.valueOf(staff.getStaffHeight());
		int weight = Integer.valueOf(staff.getStaffWeight());
		double bmi = weight / (((float)height/100)*((float)height/100));
		
//		generate Email
		String email = staff.getStaffEnglishName().replace("-", "") + "@transglobe.com.tw";
		
		logger.info("calculate BMI and generate Email done");
		
//		set BMI, Email
		staff.setStaffBMI(Double.toString(bmi));
		staff.setStaffEmail(email);
		
		return mybatisRepository.createData(staff);
	}
	
	/**
	 * 
	 * @param staff
	 * @return
	 */
	public boolean updateData(Staff staff) {
		return mybatisRepository.updateData(staff);
	}
	
	/**
	 * 
	 * @param staffID
	 * @return
	 */
	public boolean deleteData(int staffID) {
		return mybatisRepository.deleteData(staffID);
	}
	
}
