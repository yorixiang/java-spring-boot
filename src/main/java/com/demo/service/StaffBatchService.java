package com.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.bean.Staff;
import com.demo.mapper.MybatisRepository;

@Service
public class StaffBatchService {
	
	private static final Logger logger = LogManager.getLogger(StaffBatchService.class);
	
	@Autowired
	private MybatisRepository mybatisRepository;

	@Transactional
	public void batchInsert(List<String> staffArrayList) {
		
		List<Staff> staffList = new ArrayList<>();
		
		for (String raw : staffArrayList) {
			
			String[] data = raw.split(",");
			Staff staff = new Staff();
			staff.setStaffID(data[0]);
			staff.setStaffHeight(data[1]);
			staff.setStaffWeight(data[2]);
			staff.setStaffEnglishName(data[3]);
			staff.setStaffChineseNmae(data[4]);
			staff.setStaffExt(data[5]);
			staff.setStaffEmail(data[6]);
			staff.setStaffBMI(data[7]);
			staffList.add(staff);
			
			if (staffList.size() == 10) {
				mybatisRepository.batchInsert(staffList);
				staffList.clear();
				logger.info("insert 10 data to database");
				continue;
			} 
			
		}
		
		if (!staffList.isEmpty()) {
			mybatisRepository.batchInsert(staffList);
			logger.info("all data insert to database");
		}
		
	}
	
}
