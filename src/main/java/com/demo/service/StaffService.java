package com.demo.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.demo.aspect.LogExecutionTime;
import com.demo.bean.Staff;
import com.demo.repository.StaffRepository;

@CacheConfig(cacheNames = "Staffs")
@Service
public class StaffService {

	@Autowired
	StaffRepository staffRepository;
	
	private static final Logger logger = LogManager.getLogger(StaffService.class);
	
	/**
	 * read data by ID
	 * @param staffID
	 * @return
	 */
	@Cacheable(key = "#staffID")
	@LogExecutionTime
	public List<Staff> readData(int staffID) {
		return staffRepository.readData(staffID);
	}

	/**
	 * create a staff data
	 * @param staff
	 * @return
	 */
	@LogExecutionTime
	public boolean createData(Staff staff) {
		
//		calculate BMI
		int height = Integer.valueOf(staff.getStaffHeight());
		int weight = Integer.valueOf(staff.getStaffWeight());
		double bmi = weight / (((float)height/100)*((float)height/100));
		
//		generate Email
		String email = staff.getStaffEnglishName().replace("-", "") + "@transglobe.com.tw";
		
		logger.info("calculate BMI and generate Email done");
		
//		set BMI, Email
		staff.setStaffBMI(Double.toString(bmi));
		staff.setStaffEmail(email);
		
		return staffRepository.createData(staff);
	}
	
	/**
	 * update data
	 * @param staff
	 * @return
	 */
	@LogExecutionTime
	public boolean updateData(Staff staff) {
		return staffRepository.updateData(staff);
	}
	

	/**
	 * dalete data by ID
	 * @param staffID
	 * @return
	 */
	@CacheEvict(key = "#staffID")
	@LogExecutionTime
	public boolean deleteData(int staffID) {
		return staffRepository.deleteData(staffID);
	}
	
	/**
	 * download data to CSV
	 * @return
	 */
	public String downloadToCSV() {
		String staffData = null;
		StringBuilder stringBuilder = new StringBuilder();
		for (Staff staff : staffRepository.queryAll()) {
			String[] strings = {
					staff.getStaffID(),
					staff.getStaffHeight(),
					staff.getStaffWeight(),
					staff.getStaffEnglishName(),
					staff.getStaffChineseNmae(),
					staff.getStaffExt(),
					staff.getStaffEmail(),
					staff.getStaffBMI(),
					staff.getRecord().getCreateTime(),
					staff.getRecord().getUpdateTime()
			};
			staffData = String.join(",", strings);
			stringBuilder.append(staffData).append("\n");
			logger.info("add 1 data {}", staff.getStaffID());
		}
		return stringBuilder.toString();
	}
}
