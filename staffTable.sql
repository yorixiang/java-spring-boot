-- --------------------------------------------------------
-- 主機:                           10.67.67.186
-- 伺服器版本:                        5.7.27 - MySQL Community Server (GPL)
-- 伺服器操作系統:                      Linux
-- HeidiSQL 版本:                  10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 傾印 emp_yori 的資料庫結構
CREATE DATABASE IF NOT EXISTS `emp_yori` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `emp_yori`;

-- 傾印  表格 emp_yori.staffTable 結構
CREATE TABLE IF NOT EXISTS `staffTable` (
  `staffID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staffHeight` int(10) unsigned NOT NULL,
  `staffWeight` int(10) unsigned NOT NULL,
  `staffEnglishName` varchar(50) NOT NULL,
  `staffChineseName` varchar(50) NOT NULL,
  `staffExt` varchar(4) NOT NULL,
  `staffEmail` varchar(50) NOT NULL,
  `staffBMI` double unsigned NOT NULL,
  `createDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`staffID`),
  UNIQUE KEY `staffExt` (`staffExt`),
  UNIQUE KEY `staffEmail` (`staffEmail`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

-- 正在傾印表格  emp_yori.staffTable 的資料：~56 rows (約數)
DELETE FROM `staffTable`;
/*!40000 ALTER TABLE `staffTable` DISABLE KEYS */;
INSERT INTO `staffTable` (`staffID`, `staffHeight`, `staffWeight`, `staffEnglishName`, `staffChineseName`, `staffExt`, `staffEmail`, `staffBMI`, `createDateTime`, `updateDateTime`) VALUES
	(1, 181, 64, 'Edward-Liaw', '廖智強', '6720', 'edwardliaw@transglobe.com.tw', 19.54, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(2, 177, 80, 'Aaron-Huang', '黃大原', '2115', 'AaronHuang@transglobe.com.tw', 25.54, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(3, 154, 69, 'Aya-Wong', '黃美羚', '6944', 'ayawong@transglobe.com.tw', 29.09, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(4, 163, 53, 'Alvin-Chen', '陳毅鴻', '6721', 'AlvinChen@transglobe.com.tw', 19.95, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(5, 182, 88, 'Amy-Hung', '洪雯芳', '6790', 'AmyHung@transglobe.com.tw', 26.57, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(6, 160, 88, 'Annie-Wang', '王安妮', '6759', 'AnnieWang@transglobe.com.tw', 34.37, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(7, 189, 69, 'Cosmo-Hsueh', '薛祐承', '6740', 'CosmoHsueh@transglobe.com.tw', 19.32, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(8, 175, 66, 'Disen-Chen', '陳迪森', '6939', 'disenchen@transglobe.com.tw', 21.55, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(9, 151, 58, 'Douglas-Chang', '張守道', '6719', 'DouglasChang@transglobe.com.tw', 25.44, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(10, 165, 56, 'George-Gao', '高至緯', '6718', 'GeorgeGao@transglobe.com.tw', 20.57, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(11, 154, 71, 'Howard-Chang', '張哲豪', '6747', 'HowardChang@transglobe.com.tw', 29.94, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(12, 174, 75, 'James-Liu', '劉嘉航', '6743', 'JamesLiu@transglobe.com.tw', 24.77, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(13, 186, 71, 'Jeffrey-Wang', '王潘銘', '6727', 'JeffreyWang@transglobe.com.tw', 20.52, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(14, 181, 55, 'Jeremy-Cheng', '鄭唯毅', '6974', 'jeremycheng@transglobe.com.tw', 16.79, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(15, 153, 75, 'Jet-Yang', '楊杰霖', '2113', 'jetyang@transglobe.com.tw', 32.04, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(16, 185, 58, 'Jobim-Wang', '王揚凱', '6980', 'jobimwang@transglobe.com.tw', 16.95, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(17, 185, 85, 'Kate-Shen', '沈婕妤', '6737', 'KateShen@transglobe.com.tw', 24.84, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(18, 187, 52, 'Linda-Shih', '施孟秀', '2111', 'lindashih@transglobe.com.tw', 14.87, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(19, 170, 85, 'Maggie-Chen', '陳玫汝', '6927', 'maggiechen@transglobe.com.tw', 29.41, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(20, 161, 73, 'Martin-Yu', '余承澤', '6948', 'martinyu@transglobe.com.tw', 28.16, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(21, 187, 52, 'Obastan-Yang', '楊文銘', '6733', 'obastanyang@transglobe.com.tw', 14.87, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(22, 189, 72, 'Phyllis-Hsiung', '熊若芸', '6722', 'PhyllisHsiung@transglobe.com.tw', 20.16, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(23, 178, 50, 'Shaoyu-Chang', '張少宇', '6757', 'ShaoyuChang@transglobe.com.tw', 15.78, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(24, 183, 54, 'Shirley-Fu', '符聖玲', '6929', 'shirleyfu@transglobe.com.tw', 16.12, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(25, 187, 72, 'Steve-Liu', '劉俊緯', '6723', 'SteveLiu@transglobe.com.tw', 20.59, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(26, 176, 84, 'Yetta-Shieh', '謝青燕', '6726', 'yettashieh@transglobe.com.tw', 27.12, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(27, 169, 87, 'YuCheng-Lin', '林昱承', '6728', 'YuChengLin@transglobe.com.tw', 30.46, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(28, 183, 54, 'Christine-Chang', '張琬琳', '6957', 'christinechang@transglobe.com.tw', 16.12, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(29, 187, 72, 'Akino-Tsai', '蔡銘炯', '6906', 'akinotsai@transglobe.com.tw', 20.59, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(30, 176, 84, 'Amery-Yen', '嚴清莉', '6941', 'ameryyen@transglobe.com.tw', 27.12, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(31, 169, 87, 'Arthur-Chang', '張永興', '6964', 'arthurchang@transglobe.com.tw', 30.46, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(32, 151, 79, 'Benson-Yu', '游輝瓏', '2255', 'BensonYu@transglobe.com.tw', 34.65, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(33, 176, 66, 'Chiahui-Su', '蘇佳慧', '6777', 'ChiahuiSu@transglobe.com.tw', 21.31, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(34, 169, 66, 'Chili-Lu', '盧琪莉', '6977', 'chililu@transglobe.com.tw', 23.11, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(35, 178, 58, 'Colin-Hsu', '徐志銘', '6921', 'ColinHsu@transglobe.com.tw', 18.31, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(36, 160, 58, 'Curious-Wang', '王祺皓', '6774', 'CuriousWang@transglobe.com.tw', 22.66, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(37, 186, 57, 'Dania-Lin', '林秀娟', '6962', 'danialin@transglobe.com.tw', 16.48, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(38, 161, 67, 'Daniel-Chou', '周正華', '6753', 'danielchou@transglobe.com.tw', 25.85, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(39, 171, 89, 'David-Chen', '陳戴祺', '6909', 'davidchen@transglobe.com.tw', 30.44, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(40, 188, 66, 'David-Chin', '覃智誠', '6787', 'DavidChin@transglobe.com.tw', 18.67, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(41, 152, 69, 'David-Kung', '孔大維', '6741', 'DavidKung@transglobe.com.tw', 29.86, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(42, 183, 53, 'Dean-Wang', '王俊迪', '6773', 'DeanWang@transglobe.com.tw', 15.83, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(43, 168, 50, 'Debby-Pan', '潘麗慧', '6937', 'debbypan@transglobe.com.tw', 17.72, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(44, 189, 73, 'Eddy-Shin', '辛易達', '6784', 'EddyShin@transglobe.com.tw', 20.44, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(45, 160, 53, 'Ella-Wu', '吳佩珍', '6924', 'ellawu@transglobe.com.tw', 20.7, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(46, 176, 61, 'Eric-Cheng', '程福恆', '6952', 'ericcheng@transglobe.com.tw', 19.69, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(47, 171, 57, 'Eric-Lin', '林俊安', '6963', 'ericlinja@transglobe.com.tw', 19.49, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(48, 170, 55, 'Frank-Lin', '林宗宏', '6768', 'FrankLin@transglobe.com.tw', 19.03, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(49, 162, 76, 'Freddy-Chen', '陳文豪', '6791', 'FreddyChen@transglobe.com.tw', 28.96, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(50, 189, 83, 'Henry-Chang', '張宏維', '6935', 'HenryChang@transglobe.com.tw', 23.24, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(51, 166, 53, 'Hope-Pan', '潘惠貞', '6934', 'hopepan@transglobe.com.tw', 19.23, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(52, 162, 70, 'Jack-Hsu', '許良輝', '6960', 'jackhsu@transglobe.com.tw', 26.67, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(53, 164, 71, 'Jack-Tu', '涂鏡敏', '6919', 'jacktu@transglobe.com.tw', 26.4, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(54, 185, 73, 'James-Wu', '吳健民', '6907', 'jameswu@transglobe.com.tw', 21.33, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(55, 171, 58, 'Jay-Hsu', '徐民杰', '6983', 'JayHsu@transglobe.com.tw', 19.84, '2019-09-18 09:33:26', '2019-09-18 09:33:26'),
	(56, 156, 62, 'Jay-Lo', '羅傑', '6997', 'jaylo@transglobe.com.tw', 25.48, '2019-09-18 09:33:26', '2019-09-18 09:33:26');
/*!40000 ALTER TABLE `staffTable` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
